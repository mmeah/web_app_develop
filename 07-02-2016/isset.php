<?php
$var ='';
// This will evaluate to TRUE so that text will be printed.
if (isset($var)){
    echo "This is set so I will print";
}

// In the next example we'll use var_dump to output
// The retyrn value of isset().
$a = "test";
$b = "anothertest";

var_dump(isset($a)); //TRUE
var_dump(isset($a, $b)); //TRUE
echo '</br>';

unset($a);

var_dump(isset($a)); //FALSE
var_dump(isset($a, $b)); //FALSE
echo '</br>';

$foo = NULL;
var_dump(isset($foo));
echo '</br>';
?>